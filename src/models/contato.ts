export interface Contato {
    _id: string;
    Matricula: string;
    Nome: string;
    Email: string;
    Telefone: string;
    Foto: string;
    Celular: string;
    Cargo: string;
    Localizacao: string;
    Aniversario: string;
    GerenciaExecutiva: string;
    Gerencia: string;
    Interesse: string;
}
